import './App.css';
import React from 'react';
import Navbar from './component/Navbar';
import MainContent from './component/MainContent';
import DropDown from './component/DropDown';
import Paginate from "./component/Pagination";
import Loader from './component/Loader';
class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      items: [],
      totalPage: 0,
      currentPage: 0,
      searchContent:"",
      hits:0,
      timeTaken:0,
      content:"story",
      sort:"popularity"
    };
    this.DataisLoaded = false;
    this.error = false;
  }
  
  componentDidMount() {
    this.DataisLoaded = false;
    this.fetchData()
  }
  getPageContent = (event) => {
    this.fetchData(event.target.textContent - 1, this.state.searchContent,this.state.content,this.state.sort)
    }
  getSearchContent = (event)=> {
    if (event.key === 'Enter') {
      this.fetchData(this.state.currentPage, event.target.value , this.state.content, this.state.sort)
    }

  }
  filterData = (event) => {
    if (event.target.value === "popularity" || event.target.value === "date"){
      this.fetchData(this.state.currentPage, this.state.searchContent,this.state.content,event.target.value)
    }else {
      this.fetchData(this.state.currentPage, this.state.searchContent, event.target.value, this.state.sort)
    }
  }
  fetchData = (pageNumber=0,value="",tags="story",filter="popularity") => {
    this.DataisLoaded = false;
    let API = `https://hn.algolia.com/api/v1/search?query=${value}&page=${pageNumber}&tags=${tags}`;
    if(filter !== "popularity"){
      API = `https://hn.algolia.com/api/v1/search_by_date?tags=${tags}`
    }
    fetch(API)
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          items: json.hits,
          totalPages: json.nbPages,
          currentPage: json.page,
          hits: json.nbHits,
          timeTaken: json.processingTimeMS,
          content:tags,
          searchContent: value,
          sort:filter,
        });
      })
      .catch((error)=>{
        this.error = true
      })
      .finally(()=>{
        this.DataisLoaded =  true;
      })
  }
  
  render() {
    return (
      
      <>
        <Navbar searchHandler={this.getSearchContent}/>
        {!this.DataisLoaded && <Loader />}
        {!this.DataisLoaded && this.error && <h2>An error occured while fetching the API.</h2>}

        <Paginate total={this.state.totalPages} currentPage={this.state.currentPage} changePage={this.getPageContent} current={this.state.currentPage}/>
        <DropDown sortData={this.filterData} hits = {this.state.hits} time = {this.state.timeTaken}/>
        <MainContent items={this.state.items} dataState={this.state.DataisLoaded} contentType={this.state.content} data={this.state.DataisLoaded}/>
        

      </>
    );
  }
}

export default App;



/* 


<Routes>
        <Route path='/' element={<React.Fragment>
          <Navbar /> <MainContent />
        </React.Fragment>}/>

        <Route path='/story/:id' element={<React.Fragment>
          <Navbar /> <Story/>
        </React.Fragment>} />
      </Routes>
      // <div className='App'>
      //   <Navbar />
      //   <MainContent />
      // </div>

*/