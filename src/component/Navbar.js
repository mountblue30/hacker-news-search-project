import logo from '../assets/Wents News-logos_black.png';
import '../Navbar.css';


function Navbar(props) {
    return (
        <header className='header'>
            <img className="logo" src={logo} />
            <input className='searchBar' type="text" placeholder='Search stories by title..' onKeyDown={props.searchHandler}/>
        </header>
    );
}

export default Navbar;
