import React from "react";
import '../MainContent.css';
import placeholder from "../assets/placeholder-image.jpg"

function MainContent(props){
    return(
        <div className='MainContent'>
            
            {
                props.items.map((item, index) => {
                    if(props.contentType === "story"){
                        if (item.title && item.url) {
                            return(
                                <a href={item.url} key={index}>
                                    <div className="container">
                                        <div className='story' >
                                            <div className='story-heading'>

                                                <h2>{item.title}</h2>
                                                <a href={item.url} className="url">({item.url.slice(0, 40)})</a>
                                                <span>Created at: {item.created_at}</span>
                                            </div>
                                            <div className='story-footer'>
                                                <p> {item.points} points | {item.author} | {item.num_comments} comments</p>
                                            </div>
                                        </div>
                                        <img className="placeholder" src={placeholder} />
                                    </div>
                                </a>
                                
                            )
                            
                            
                        }
                    } else {
                        return (
                            <div className="container" >
                                <div className='story' >
                                    <div className='story-heading'>

                                        <h2>{item.story_title}</h2>
                                        <h3>{item.author}</h3>

                                        <span>Created at: {item.created_at}</span>
                                    </div>
                                    <p>{item.comment_text.replace(/<\s*br[^>]?>/, '\n')
                                        .replace(/(<([^>]+)>)/g, "")}</p>
                                    <div className='story-footer'>
                                        <p> {item.points} points | {item.author} |</p>
                                    </div>
                                </div>
                                
                            </div>
                        )
                    }
                    
                })
            }
        </div>
        
    )
}
export default MainContent;
