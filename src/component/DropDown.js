import '../DropDown.css';


function DropDown(props) {
    return (
        <div className='dropdown'>  
            <label>Search</label>
            <select name="Type" id="search" onChange={props.sortData}>
                <option value="story">Stories</option>
                <option value="comment">Comments</option>
            </select>
            <label>by</label>
            <select name="by" id="by" onChange={props.sortData}>
                <option value="popularity">Popularity</option>
                <option value="date">Date</option>
            </select>
            

            <p>{props.hits.toLocaleString()} results in <span>({props.time / 1000} seconds)</span> </p>
        </div>
        
    )
}


export default DropDown;
