import { render } from "@testing-library/react";
import React from "react";
import '../Paginate.css';

function Paginate(props){
    return(
        <div className="Paginate-container">
            <div className="Paginate">
                {Array.from({ length: 10 }, (_, index) => index + 1).map((n,index) => {
                    return <h3 key={index} className={props.current == index ? "active": ""} onClick={props.changePage}>{n}</h3>
                })}
            </div>
            
        </div>
        
        
    )
}

export default Paginate;