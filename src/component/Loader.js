import "../loader.css"


function Loader(){
    return (
        <div class="box-4">
            <div class="loader-4">
                <div class="dbl-spin-1"></div>
                <div class="dbl-spin-2"></div>
            </div>
        </div>
    )
}

export default Loader;
